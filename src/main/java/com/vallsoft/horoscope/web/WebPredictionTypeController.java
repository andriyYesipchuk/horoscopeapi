package com.vallsoft.horoscope.web;

import com.vallsoft.horoscope.entity.PredictionType;
import com.vallsoft.horoscope.entity.User;
import com.vallsoft.horoscope.repository.PredictionTypeRepository;
import com.vallsoft.horoscope.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class WebPredictionTypeController {

    @Autowired
    private PredictionTypeRepository predictionTypeRepository;

    @Autowired
    private UserRepository userRepository;


    @RequestMapping("/getAllTypes")
    public String getAllTypes(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user = userRepository.findUserByUsername(userDetails.getUsername());

        List<PredictionType> types = predictionTypeRepository.findAll();

        model.addAttribute("items", types);
        model.addAttribute("user", user);
        return "prediction_types  :: types-table";
    }

    @RequestMapping(value = "/getType")
    public String getType(@AuthenticationPrincipal UserDetails userDetails, Model model,
                          @RequestParam(value = "id") long id) {
        Optional<PredictionType> optionalType = predictionTypeRepository.findById(id);

        if (optionalType.isPresent()) {
            model.addAttribute("item", optionalType.get());
        }
        return "prediction_types :: updTypeModal";
    }

    @RequestMapping(value = "/addType", method = RequestMethod.POST)
    public String addType(@AuthenticationPrincipal UserDetails userDetails, Model model,
                          @RequestParam(value = "string") String string) {
        PredictionType type = new PredictionType();
        type.setName(string);
        PredictionType saved = predictionTypeRepository.save(type);
        return getAllTypes(userDetails, model);
    }

    @RequestMapping(value = "/updateType", method = RequestMethod.POST)
    public String updateType(@AuthenticationPrincipal UserDetails userDetails, Model model,
                             @RequestParam(value = "id") long id,
                             @RequestParam(value = "string") String string) {
        Optional<PredictionType> optionalType = predictionTypeRepository.findById(id);
        if (optionalType.isPresent()) {
            PredictionType type = optionalType.get();
            type.setName(string);
            PredictionType saved = predictionTypeRepository.save(type);
        }

        return getAllTypes(userDetails, model);
    }

    @RequestMapping(value = "/deleteType", method = RequestMethod.POST)
    public String deleteType(@AuthenticationPrincipal UserDetails userDetails, Model model,
                             @RequestParam(value = "id") long id) {
        Optional<PredictionType> optionalType = predictionTypeRepository.findById(id);
        if (optionalType.isPresent()) {
            predictionTypeRepository.delete(optionalType.get());
        }
        return getAllTypes(userDetails, model);
    }

}
