package com.vallsoft.horoscope.service;

import com.vallsoft.horoscope.entity.Role;
import com.vallsoft.horoscope.repository.RoleRepository;
import com.vallsoft.horoscope.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoleService {

    @Autowired
    RoleRepository roleRepository;



    public ApiResponse<List<Role>> findAll(){
        List<Role> list =  roleRepository.findAll();
        return new ApiResponse<List<Role>>(1,list, null);
    }

    public ApiResponse<Role> findById(long id){
        Optional<Role> optionalCategoru = roleRepository.findById(id);
        if (optionalCategoru.isPresent()){
            return new ApiResponse<Role>(1,optionalCategoru.get(),null);
        }
        else {
            return new ApiResponse<Role>(0,null,"Categoru with id = "+id+" not found!");
        }
    }

    public ApiResponse<Role> deleteById(long id){

        Optional<Role> optionalCategoru = roleRepository.findById(id);
        if (optionalCategoru.isPresent()){
            roleRepository.deleteById(id);
            return new ApiResponse<Role>(1,null,null);
        }
        else {
            return new ApiResponse<Role>(0,null,"Categoru with id = "+id+" not found!");
        }
    }

    public ApiResponse<Role> save(Role role){
        if (role==null){
            return new ApiResponse<Role>(0,null,"category is not present!");
        }
        else if (role.getId()==0){
            Role saved = roleRepository.save(role);
            return new ApiResponse<Role>(1,saved,null);
        }
        else {
            Role saved = roleRepository.saveAndFlush(role);
            return new ApiResponse<Role>(1,saved,null);
        }
    }
}
