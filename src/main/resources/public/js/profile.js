$(document).ready(function () {

    $(document).on( 'click', "#addPowerUser", function(e) {
        console.log('button'+ "#changeAdvertisement");
        var username = document.getElementById("chosenUser").innerHTML;
        username = username.substring(username.indexOf(":")+2);
        console.log("/addPowerUser?username="+username);
        $.ajax({
            type:"post",
            data: {username:username},
            url: "/addPowerUser",
            success: function(response) {
                console.log("success");
                $('#power-users-table').replaceWith(response);
                $('#findSuperUsers').modal('hide');
                //window.alert(response);
            }
        });
    });

    $(document).on( 'click', "#changeAdvertisement", function(e) {
        console.log('button'+ "#changeAdvertisement");
        var value="";
        if(document.getElementById("radioRound").checked){
            value = "ROUND";
        }
        if(document.getElementById("radioGame").checked){
            value = "GAME";
        }
        console.log("/saveAdvertisementSettings");
        console.log(value);
        $.ajax({
            type:"post",
            data: {value:value},
            url: "/saveAdvertisementSettings",
            success: function(response) {
                console.log("success");

                $('#advertisementModal').modal('hide');
            }
        });
    });

    $(document).on( 'click', "#changeUserAgreement", function(e) {
        console.log('button'+ "#changeUserAgreement");
        var value=document.getElementById("userAgreementTA").value;
        console.log("/saveUserAgreement");
        console.log(value);
        $.ajax({
            type:"post",
            data: {value:value},
            url: "/saveUserAgreement",
            success: function(response) {
                console.log("success");

                $('#userAgreementModal').modal('hide');
            }
        });
    });

});

function findUsersByUsername() {
    var username = document.getElementById("usernameInput").value;
    console.log("/findUsersByUsername");
    $.ajax({
        type:"post",
        data: {username:username},
        url: "/findUsersByUsername",
        success: function(response) {
            console.log("success");
            $('#foundUsers').replaceWith(response);
        }
    });
}

function choosePowerUser(username) {
    console.log("onclick:choosePowerUser("+username+")");
    document.getElementById("chosenUser").innerHTML = "Выбрано пользователя: "+username;
}

function deletePowerUser(id) {
    console.log("/deletePowerUser");
    $.ajax({
        type:"post",
        data: {id:id},
        url: "/deletePowerUser",
        success: function(response) {
            console.log("success");
            $('#power-users-table').replaceWith(response);
        }
    });
}

function showAdvertisementSettings() {

    console.log("/getAdvertisementSettings");
    $.ajax({
        type:"get",
        url: "/getAdvertisementSettings",
        success: function(response) {
            console.log("success");
            $('#advertisementModal').replaceWith(response);
            $('#advertisementModal').modal();
        }
    });
}

function showUserAgreement() {

    console.log("/getUserAgreement");
    $.ajax({
        type:"get",
        url: "/getUserAgreement",
        success: function(response) {
            console.log("success");
            $('#userAgreementModal').replaceWith(response);
            $('#userAgreementModal').modal();
        }
    });
}


