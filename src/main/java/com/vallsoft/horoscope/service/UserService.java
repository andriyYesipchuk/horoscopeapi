package com.vallsoft.horoscope.service;

import com.vallsoft.horoscope.entity.User;
import com.vallsoft.horoscope.repository.RoleRepository;
import com.vallsoft.horoscope.repository.UserRepository;
import com.vallsoft.horoscope.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@Transactional
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public ApiResponse<User> save(User user){
        if (user == null) return new ApiResponse<User>(0,null,"User is not present!");
        if (user.getId()==0){
            return null;
        }
        else{

            User saved = userRepository.saveAndFlush(user);
            return new ApiResponse<User>(1, saved, null);
        }
    }

    public ApiResponse<User> register(String username, String email, String password, String professionName) {
        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);

        User alreadyExistsByUsername, alreadyExistsByEmail;
        alreadyExistsByEmail = userRepository.findUserByEmail(user.getEmail());
        alreadyExistsByUsername = userRepository.findUserByUsername(user.getUsername());
        String error = "";
        if (alreadyExistsByUsername != null) {
            error += "Пользователь с таким логином уже зарегистрован!";
        }
        if (alreadyExistsByEmail != null) {
            error += "Пользователь с таким email уже зарегистрован!";
        }

        if (!error.isEmpty()) {
            return new ApiResponse<User>(0, null, error);
        } else {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);


            String confirmationPin = "";
            for (int i = 0; i < 4; i++) {
                int number = new Random().nextInt(10);
                confirmationPin += number;
            }

            user.setEnabled(true);
            user.setCreationdate(new Date());

            User saved = userRepository.save(user);

            return new ApiResponse<User>(1, saved, null);
        }
    }

    public ApiResponse<User> login(String username, String password){
        User user = userRepository.findUserByUsername(username);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (user!=null && passwordEncoder.matches(password,user.getPassword())){
            return new ApiResponse<User>(1, user, null);
        }
        return new ApiResponse<User>(0, null, "Неправильный логин или пароль!");
    }

    public ApiResponse<User> deleteById(long id){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setEnabled(false);
            User saved = userRepository.save(user);
            return new ApiResponse<User>(1,null,null);
        }
        return new ApiResponse<User>(0,null,"User with id = "+id+" not found!");
    }

    public ApiResponse<List<User>> findAll(){
        List<User> list = userRepository.findAll();
        return new ApiResponse<List<User>>(1,list,null);
    }

    public ApiResponse<User> findById(long id){

        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            return new ApiResponse<User>(1,user,null);
        }
        return new ApiResponse<User>(0,null,"User with id = "+id+" not found!");
    }

    public ApiResponse<List<User>> findUsersByUsername(String username, long id){
        username = "%"+username+"%";
        List<User> list = userRepository.findUsersByUsernameLike(username);
        if (list.size()>10)
            list = list.subList(0,10);
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            list.remove(optionalUser.get());

        }
        return new ApiResponse<List<User>>(1,list,null);
    }

    public ApiResponse<User> updateInfo(long id, String username, String email, String password){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            if (!username.isEmpty()){
                user.setUsername(username);
            }
            if(!email.isEmpty()){
                user.setEmail(email);
            }
            if(!password.isEmpty()){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                String hashedPassword = passwordEncoder.encode(password);

                user.setPassword(hashedPassword);
            }
            User saved = userRepository.save(user);
            return new ApiResponse<User>(1,saved,null);
        }
        else {
            return new ApiResponse<User>(0,null,"Пользователя не найдено!");
        }
    }
}
