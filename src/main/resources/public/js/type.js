$(document).ready(function () {

    $(document).on('click', "#updType", function (e) {
        console.log('button' + "#updType");
        var id = $('#update-type').attr("typeId");
        var string = document.getElementById("stringUPD").value;
        if (isValid(string)) {
            console.log("/updateType");
            $.ajax({
                url: '/updateType',
                type: 'POST',
                data: {id: id, string: string},
                success: function (response) {
                    console.log("success");
                    $("#types-table").replaceWith(response);
                    $('#updTypeModal').modal('hide');
                }
            });
        }
        else {
            window.alert("Заполните, пожалуйста, все поля!");
        }
    });

    $(document).on('click', "#addType", function (e) {
        console.log('button' + "#addType");
        var string = document.getElementById("string").value;
        if (isValid(string)) {
            console.log("/addType");
            $.ajax({
                url: '/addType',
                type: 'POST',
                data: {string: string},
                success: function (response) {
                    console.log("success");
                    console.log(response);
                    $("#types-table").replaceWith(response);
                    $('#addTypeModal').modal('hide');
                }
            });
        }
        else {
            window.alert("Заполните, пожалуйста, все поля!");
        }
    });
});

function isValid(text) {
    if (text != undefined && text != "" && text.length <= 255) {
        return true;
    }
    else {
        return false;
    }
}

function viewType(id) {
    $.ajax({
        type: "get",
        data: {id: id},
        url: "/getType",
        success: function (response) {
            $('#updTypeModal').replaceWith(response);
            $('#updTypeModal').modal();
        }
    });
}

function deleteType(id) {
    $.ajax({
        type: "POST",
        data: {id: id},
        url: "/deleteType",
        success: function (response) {
            console.log("success");
            $('#types-table').replaceWith(response);
        }
    });
}