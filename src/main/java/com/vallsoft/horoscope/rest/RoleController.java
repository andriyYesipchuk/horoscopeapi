package com.vallsoft.horoscope.rest;

import com.vallsoft.horoscope.entity.Role;
import com.vallsoft.horoscope.response.ApiResponse;
import com.vallsoft.horoscope.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/roles/")
public class RoleController {

    @Autowired
    RoleService roleService;

    @RequestMapping
    public ApiResponse<List<Role>> readAll(){
        return roleService.findAll();
    }

    @RequestMapping("get/")
    public ApiResponse<Role> readOne(@RequestParam int id){
        return roleService.findById(id);

    }

    @RequestMapping(value = "create/", method = RequestMethod.POST)
    public ApiResponse<Role> create(@RequestBody Role role) {
        return roleService.save(role);

    }

    @RequestMapping(value = "update/", method = RequestMethod.PUT)
    public ApiResponse<Role> update(@RequestBody Role role){
        return roleService.save(role);

    }

    @RequestMapping(value = "delete/", method = RequestMethod.DELETE)
    public ApiResponse<Role> delete(@RequestParam int id){
        return roleService.deleteById(id);
    }
}
