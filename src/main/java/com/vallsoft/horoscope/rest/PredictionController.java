package com.vallsoft.horoscope.rest;

import com.vallsoft.horoscope.entity.Prediction;
import com.vallsoft.horoscope.entity.PredictionType;
import com.vallsoft.horoscope.repository.PredictionRepository;
import com.vallsoft.horoscope.repository.PredictionTypeRepository;
import com.vallsoft.horoscope.response.ApiResponse;
import com.vallsoft.horoscope.response.FullPrediction;
import com.vallsoft.horoscope.service.PredictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/prediction/")
public class PredictionController {

    @Autowired
    PredictionService predictionService;

    @Autowired
    PredictionRepository predictionRepository;

    @Autowired
    PredictionTypeRepository predictionTypeRepository;

    @RequestMapping
    public ApiResponse<List<Prediction>> readAll() {
        return predictionService.findAll();
    }

    @RequestMapping("full/")
    public ApiResponse<FullPrediction> fullPrediction() {
        List<PredictionType> types = predictionTypeRepository.findAll();
        List<Prediction> predictions = new ArrayList<>();

        for (int index = 0; index < types.size(); index++) {
            PredictionType type = types.get(index);
            List<Prediction> tempPredictions = predictionRepository.getAllByType(type);
            if (tempPredictions.size() > 0) {
                int rand = (int) (Math.random() * tempPredictions.size());
                predictions.add(tempPredictions.get(rand));
            } else {
                types.remove(index);
                index--;
            }
        }

        return new ApiResponse<>(1, new FullPrediction(types, predictions), null);
    }

    @RequestMapping("get/")
    public ApiResponse<Prediction> readOne(@RequestParam int id) {
        return predictionService.findById(id);

    }

    @RequestMapping(value = "create/", method = RequestMethod.POST)
    public ApiResponse<Prediction> create(@RequestBody Prediction prediction) {
        return predictionService.save(prediction);

    }

    @RequestMapping(value = "update/", method = RequestMethod.PUT)
    public ApiResponse<Prediction> update(@RequestBody Prediction prediction) {
        return predictionService.save(prediction);

    }

    @RequestMapping(value = "delete/", method = RequestMethod.DELETE)
    public ApiResponse<Prediction> delete(@RequestParam int id) {
        return predictionService.deleteById(id);
    }
}
