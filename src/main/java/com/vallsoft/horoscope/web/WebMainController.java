package com.vallsoft.horoscope.web;


import com.vallsoft.horoscope.entity.Prediction;
import com.vallsoft.horoscope.entity.PredictionType;
import com.vallsoft.horoscope.entity.Role;
import com.vallsoft.horoscope.entity.User;
import com.vallsoft.horoscope.repository.PredictionRepository;
import com.vallsoft.horoscope.repository.PredictionTypeRepository;
import com.vallsoft.horoscope.repository.RoleRepository;
import com.vallsoft.horoscope.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class WebMainController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PredictionRepository predictionRepository;

    @Autowired
    private PredictionTypeRepository predictionTypeRepository;

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/users")
    public String users_access(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user =  userRepository.findUserByUsername(userDetails.getUsername());
        List<User> users = userRepository.findAllByOrderByUsername();
        model.addAttribute("users",users);
        model.addAttribute("user",user);
        return "users";
    }

    @RequestMapping("/profile")
    public String profile(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user =  userRepository.findUserByUsername(userDetails.getUsername());
        Role role = roleRepository.findRoleByName("POWER_USER");
        List<User> powerUsers = new ArrayList<>();
        if (role!=null){
            powerUsers = userRepository.findUsersByRolesContains(role);
        }
        if (powerUsers==null) powerUsers = new ArrayList<>();
        model.addAttribute("powerUsers",powerUsers);
        model.addAttribute("user",user);
        return "profile";
    }

    @RequestMapping("/predictions")
    public String new_questions(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user = userRepository.findUserByUsername(userDetails.getUsername());

        List<Prediction> predictions = predictionRepository.findAll();
        List<PredictionType> predictionsTypes = predictionTypeRepository.findAll();
        model.addAttribute("items", predictions);
        model.addAttribute("user", user);
        model.addAttribute("prediction_types", predictionsTypes);
        return "predictions";
    }

    @RequestMapping("/prediction_types")
    public String categories(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user = userRepository.findUserByUsername(userDetails.getUsername());

        List<PredictionType> predictionTypes = predictionTypeRepository.findAll();
        model.addAttribute("items", predictionTypes);
        model.addAttribute("user", user);
        return "prediction_types";
    }
}
