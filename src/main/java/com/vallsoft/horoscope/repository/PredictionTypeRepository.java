package com.vallsoft.horoscope.repository;

import com.vallsoft.horoscope.entity.PredictionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PredictionTypeRepository extends JpaRepository<PredictionType, Long> {

    PredictionType findPredictionTypeByName(String name);
}
