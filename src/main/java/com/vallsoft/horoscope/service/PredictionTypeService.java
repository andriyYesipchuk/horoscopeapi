package com.vallsoft.horoscope.service;

import com.vallsoft.horoscope.entity.PredictionType;
import com.vallsoft.horoscope.repository.PredictionTypeRepository;
import com.vallsoft.horoscope.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PredictionTypeService {

    @Autowired
    PredictionTypeRepository predictionTypeRepository;


    public ApiResponse<List<PredictionType>> findAll() {
        List<PredictionType> list = predictionTypeRepository.findAll();
        return new ApiResponse<List<PredictionType>>(1, list, null);
    }

    public ApiResponse<PredictionType> findById(long id) {
        Optional<PredictionType> optionalPredictionType = predictionTypeRepository.findById(id);
        if (optionalPredictionType.isPresent()) {
            return new ApiResponse<PredictionType>(1, optionalPredictionType.get(), null);
        } else {
            return new ApiResponse<PredictionType>(0, null, "Prediction type with id = " + id + " not found!");
        }
    }

    public ApiResponse<PredictionType> deleteById(long id) {

        Optional<PredictionType> optionalCategoru = predictionTypeRepository.findById(id);
        if (optionalCategoru.isPresent()) {
            predictionTypeRepository.deleteById(id);
            return new ApiResponse<PredictionType>(1, null, null);
        } else {
            return new ApiResponse<PredictionType>(0, null, "Prediction type with id = " + id + " not found!");
        }
    }

    public ApiResponse<PredictionType> save(PredictionType predictionType) {
        if (predictionType == null) {
            return new ApiResponse<PredictionType>(0, null, "category is not present!");
        } else if (predictionType.getId() == 0) {
            PredictionType saved = predictionTypeRepository.save(predictionType);
            return new ApiResponse<PredictionType>(1, saved, null);
        } else {
            PredictionType saved = predictionTypeRepository.saveAndFlush(predictionType);
            return new ApiResponse<PredictionType>(1, saved, null);
        }
    }
}
