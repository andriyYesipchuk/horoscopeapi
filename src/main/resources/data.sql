INSERT INTO public.role ( id, description,  name) VALUES ( 1, 'Admin', 'ADMIN') ON CONFLICT DO NOTHING ;
INSERT INTO public.role ( id, description,  name) VALUES ( 2, 'Moderator',  'POWER_USER') ON CONFLICT DO NOTHING ;

INSERT INTO public."user"( id,enabled, firstname, lastname,  password, username) VALUES ( 1, 'TRUE', 'admin', 'admin',  '$2a$10$gNynjW1qLU3zkWwq2yOOiOTTC6HkhMyyf3.TnG9nYW.PiyeTCIKG2', 'admin') ON CONFLICT DO NOTHING ;

INSERT INTO public.user_roles
    (user_id, role_id)
SELECT 1 as "user_id", 1 as "role_id"
WHERE
    NOT EXISTS (
          SELECT user_id FROM public.user_roles WHERE user_id = 1 and role_id =1
        );

INSERT INTO public.user_roles
    (user_id, role_id)
SELECT 1 as "user_id", 2 as "role_id"
WHERE
    NOT EXISTS (
          SELECT user_id FROM public.user_roles WHERE user_id = 1 and role_id =2
        );

INSERT INTO public.prediction_type ( id, name) VALUES ( 1, 'Общий') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 2, 'Бизнес') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 3, 'Здоровье') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 4, 'Эротический') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 5, 'Любовный') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 6, 'Кулинарный') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 7, 'Мобильный') ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction_type ( id, name) VALUES ( 8, 'Антигороскоп') ON CONFLICT DO NOTHING ;

INSERT INTO public.prediction ( id, text, type_id) VALUES ( 1, 'Общее предсказание', 1) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 2, 'Бизнес предсказание', 2) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 3, 'Здоровье предсказание', 3) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 4, 'Эротическое предсказание', 4) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 5, 'Любовное предсказание', 5) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 6, 'Кулинарное предсказание', 6) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 7, 'Мобильное предсказание', 7) ON CONFLICT DO NOTHING ;
INSERT INTO public.prediction ( id, text, type_id) VALUES ( 8, 'Антигороскоп предсказание', 8) ON CONFLICT DO NOTHING ;