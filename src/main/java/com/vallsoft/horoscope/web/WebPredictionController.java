package com.vallsoft.horoscope.web;

import com.vallsoft.horoscope.entity.Prediction;
import com.vallsoft.horoscope.entity.PredictionType;
import com.vallsoft.horoscope.entity.User;
import com.vallsoft.horoscope.repository.PredictionRepository;
import com.vallsoft.horoscope.repository.PredictionTypeRepository;
import com.vallsoft.horoscope.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class WebPredictionController {
    @Autowired
    private PredictionRepository predictionRepository;

    @Autowired
    private PredictionTypeRepository predictionTypeRepository;

    @Autowired
    private UserRepository userRepository;


    @RequestMapping("/getAllPredictions")
    public String getAllPredictions(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user = userRepository.findUserByUsername(userDetails.getUsername());

        List<Prediction> predictions = predictionRepository.findAll();

        model.addAttribute("items", predictions);
        model.addAttribute("user", user);
        return "predictions  :: predictions-table";
    }

    @RequestMapping(value = "/getPrediction")
    public String getPrediction(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                @RequestParam(value = "id") long id) {
        Optional<Prediction> optionalPrediction = predictionRepository.findById(id);
        List<PredictionType> typeList = predictionTypeRepository.findAll();
        if (typeList == null) {
            typeList = new ArrayList<>();
        }

        if (optionalPrediction.isPresent()) {
            model.addAttribute("item", optionalPrediction.get());
            typeList.remove(optionalPrediction.get().getType());
            model.addAttribute("prediction_types", typeList);
        }
        return "predictions :: updPredictionModal";
    }

    @RequestMapping(value = "/addPrediction", method = RequestMethod.POST)
    public String addPrediction(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                @RequestParam(value = "text") String text,
                                @RequestParam(value = "typeId") long typeId) {
        Prediction prediction = new Prediction();
        prediction.setText(text);
        Optional<PredictionType> optionalType = predictionTypeRepository.findById(typeId);
        if (optionalType.isPresent()) {
            prediction.setType(optionalType.get());
        }
        Prediction saved = predictionRepository.save(prediction);
        return getAllPredictions(userDetails, model);
    }

    @RequestMapping(value = "/updatePrediction", method = RequestMethod.POST)
    public String updatePrediction(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                   @RequestParam(value = "id") long id,
                                   @RequestParam(value = "text") String text,
                                   @RequestParam(value = "typeId") long typeId) {
        Optional<Prediction> optionalPrediction = predictionRepository.findById(id);
        if (optionalPrediction.isPresent()) {
            Prediction prediction = optionalPrediction.get();
            prediction.setText(text);
            Optional<PredictionType> optionalType = predictionTypeRepository.findById(typeId);
            if (optionalType.isPresent()) {
                prediction.setType(optionalType.get());
            }
            Prediction saved = predictionRepository.save(prediction);
        }

        return getAllPredictions(userDetails, model);
    }

    @RequestMapping(value = "/deletePrediction", method = RequestMethod.POST)
    public String deletePrediction(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                   @RequestParam(value = "id") long id) {
        Optional<Prediction> optionalPrediction = predictionRepository.findById(id);
        if (optionalPrediction.isPresent()) {
            predictionRepository.delete(optionalPrediction.get());
        }
        return getAllPredictions(userDetails, model);
    }

}
