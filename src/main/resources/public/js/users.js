$(document).ready(function () {

    $(document).on( 'click', "#updUser", function(e) {
        console.log('button'+ "#updUser");
        var username = document.getElementById("usernameUPD").value;
        var email = document.getElementById("emailUPD").value;
        var professionId = document.getElementById("usersProfessionSelect").value;
        var id = $('#upd-user').attr("userId");
        var enabled = document.getElementById("statusUPD").checked;
        console.log("enabled = "+enabled);
        console.log("professionId = "+professionId);
        console.log("/saveUser");
        $.ajax({
            type:"post",
            data:{id:id,username:username,email:email,enabled:enabled,professionId:professionId},
            url: "/saveUser",
            success: function(response) {
                console.log("success");
                $('#users-table').replaceWith(response);
                $('#updUserModal').modal('hide');
                //window.alert(response);
            }
        });
    });

});


function sortUsers(order) {

    document.getElementById("arrow1").style.color = "grey";
    document.getElementById("arrow2").style.color = "grey";
    document.getElementById("arrow3").style.color = "grey";
    document.getElementById("arrow4").style.color = "grey";
    document.getElementById("arrow5").style.color = "grey";
    document.getElementById("arrow6").style.color = "grey";
    document.getElementById("arrow7").style.color = "grey";
    document.getElementById("arrow8").style.color = "grey";
    document.getElementById("arrow9").style.color = "grey";
    document.getElementById("arrow10").style.color = "grey";
    var currentArrow = "arrow"+order;
    document.getElementById(currentArrow).style.color = "#57c557";
    document.getElementById("searchedUsername").value="";

    console.log("/getOrderedUsers?order="+order);
    $.ajax({
        type:"get",
        data:{order:order},
        url: "/getOrderedUsers",
        success: function(response) {
            console.log("success");
            $('#users-table').replaceWith(response);
            //window.alert(response);
        }
    });
}

function searchUsersByUsername() {
    var username = document.getElementById("searchedUsername").value;
    if (username==undefined) {
        username="";
    }

    $.ajax({
        type:"get",
        data:{username:username},
        url: "/searchUsers",
        success: function(response) {
            console.log("success");
            $('#users-table').replaceWith(response);
            //window.alert(response);
        }
    });
}

function deleteUser(id) {

    if (id>0){
        $.ajax({
            type:"post",
            data:{id:id},
            url: "/deleteUser",
            success: function(response) {
                console.log("success");
                $('#users-table').replaceWith(response);
                //window.alert(response);
            }
        });
    }
}

function viewUser(id) {


    if (id>0){
        $.ajax({
            type:"get",
            data:{id:id},
            url: "/getUser",
            success: function(response) {
                console.log("success");
                $('#updUserModal').replaceWith(response);
                $('#updUserModal').modal();
                //window.alert(response);
            }
        });
    }
}