package com.vallsoft.horoscope.repository;

import com.vallsoft.horoscope.entity.Role;
import com.vallsoft.horoscope.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByUsername(String username);

    User findUserByEmail(String email);

    List<User> findAllByOrderByCreationdateAsc();

    List<User> findUsersByRolesContains(Role role);

    @Query("SELECT user FROM User user WHERE UPPER(user.username) LIKE UPPER(:username) ORDER BY user.username ")
    List<User> findUsersByUsernameLike(@Param("username") String username);

    List<User> findAllByOrderByUsername();

    Long countAllBy();


}
