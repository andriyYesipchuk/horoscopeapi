package com.vallsoft.horoscope.service;

import com.vallsoft.horoscope.entity.Prediction;
import com.vallsoft.horoscope.repository.PredictionRepository;
import com.vallsoft.horoscope.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PredictionService {

    @Autowired
    PredictionRepository predictionRepository;

    public ApiResponse<List<Prediction>> findAll() {
        List<Prediction> list = predictionRepository.findAll();
        return new ApiResponse<List<Prediction>>(1, list, null);
    }

    public ApiResponse<Prediction> findById(long id) {
        Optional<Prediction> optionalPredictionType = predictionRepository.findById(id);
        if (optionalPredictionType.isPresent()) {
            return new ApiResponse<Prediction>(1, optionalPredictionType.get(), null);
        } else {
            return new ApiResponse<Prediction>(0, null, "Prediction type with id = " + id + " not found!");
        }
    }

    public ApiResponse<Prediction> deleteById(long id) {

        Optional<Prediction> optionalCategoru = predictionRepository.findById(id);
        if (optionalCategoru.isPresent()) {
            predictionRepository.deleteById(id);
            return new ApiResponse<Prediction>(1, null, null);
        } else {
            return new ApiResponse<Prediction>(0, null, "Prediction type with id = " + id + " not found!");
        }
    }

    public ApiResponse<Prediction> save(Prediction prediction) {
        if (prediction == null) {
            return new ApiResponse<Prediction>(0, null, "category is not present!");
        } else if (prediction.getId() == 0) {
            Prediction saved = predictionRepository.save(prediction);
            return new ApiResponse<Prediction>(1, saved, null);
        } else {
            Prediction saved = predictionRepository.saveAndFlush(prediction);
            return new ApiResponse<Prediction>(1, saved, null);
        }
    }
}
