$(document).ready(function () {

    $(document).on('click', "#updPrediction", function (e) {
        console.log('button' + "#updPrediction");
        var id = $('#update-prediction').attr("predictionId");
        var text = document.getElementById("textUPD").value;
        var typeId = document.getElementById("typeSelectUPD").value;
        if (isValid(text, typeId)) {
            console.log("/updatePrediction");
            $.ajax({
                url: '/updatePrediction',
                type: 'POST',
                data: {id: id, text: text, typeId: typeId},
                success: function (response) {
                    console.log("success");
                    console.log(response);
                    $("#predictions-table").replaceWith(response);
                    $('#updPredictionModal').modal('hide');
                }
            });
        }
        else {
            window.alert("Заполните, пожалуйста, все поля!");
        }
    });

    $(document).on('click', "#addPrediction", function (e) {
        console.log('button' + "#addPrediction");
        var text = document.getElementById("text").value;
        var typeId = document.getElementById("typeSelect").value;
        if (isValid(text, typeId)) {
            console.log("/addPrediction");
            $.ajax({
                url: '/addPrediction',
                type: 'POST',
                data: {text: text, typeId: typeId},
                success: function (response) {
                    console.log("success");
                    console.log(response);
                    $("#predictions-table").replaceWith(response);
                    $('#addPredictionModal').modal('hide');
                }
            });
        }
        else {
            window.alert("Заполните, пожалуйста, все поля!");
        }
    });
});

function isValid(text, typeId) {
    if (typeId != 0 && text != undefined && text != "" && text.length <= 255) {
        return true;
    }
    else {
        return false;
    }
}

function viewPrediction(id) {
    $.ajax({
        type:"get",
        data: {id:id},
        url: "/getPrediction",
        success: function(response) {
            $('#updPredictionModal').replaceWith(response);
            $('#updPredictionModal').modal();
        }
    });
}

function deletePrediction(id) {

    $.ajax({
        type:"POST",
        data:{id:id},
        url: "/deletePrediction",
        success: function(response) {
            console.log("success");
            console.log(response);
            $('#predictions-table').replaceWith(response);
            //window.alert(response);
        }
    });
}