package com.vallsoft.horoscope.web;

import com.vallsoft.horoscope.entity.Role;
import com.vallsoft.horoscope.entity.User;
import com.vallsoft.horoscope.repository.RoleRepository;
import com.vallsoft.horoscope.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Controller
public class WebUserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(value = "/findUsersByUsername", method = RequestMethod.POST)
    public String findUsersByUsername(@AuthenticationPrincipal UserDetails userDetails, Model model,
                               @RequestParam String username) {
        User user =  userRepository.findUserByUsername(userDetails.getUsername());

        List<User> foundUsers = new ArrayList<>();
        if (username!=null && !username.isEmpty()){
            username = '%'+username+'%';
            foundUsers = userRepository.findUsersByUsernameLike(username);
            foundUsers.remove(user);
            foundUsers.sort(Comparator.comparing(u->u.getUsername()));
        }

        model.addAttribute("foundUsers",foundUsers);
        //model.addAttribute("user",user);
        return "profile :: foundUsers";
    }


    @RequestMapping(value = "/addPowerUser", method = RequestMethod.POST)
    public String addPowerUser(@AuthenticationPrincipal UserDetails userDetails, Model model,
                               @RequestParam String username) {
        User loginUser = userRepository.findUserByUsername(userDetails.getUsername());
        User user =  userRepository.findUserByUsername(username);
        Role role =  roleRepository.findRoleByName("POWER_USER");
        if (user!=null && role!=null && !user.getRoles().contains(role)){
            user.getRoles().add(role);
            User saved = userRepository.save(user);
        }

        List<User> powerUsers = new ArrayList<>();
        if (role!=null){
            powerUsers = userRepository.findUsersByRolesContains(role);
        }
        if (powerUsers==null) powerUsers = new ArrayList<>();
        model.addAttribute("powerUsers",powerUsers);
        model.addAttribute("user",loginUser);
        return "profile :: power-users-table";
    }

    @RequestMapping(value = "/deletePowerUser", method = RequestMethod.POST)
    public String deletePowerUser(@AuthenticationPrincipal UserDetails userDetails, Model model,
                               @RequestParam long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            Role role = roleRepository.findRoleByName("POWER_USER");
            if (role!=null){
                User user = optionalUser.get();
                user.getRoles().remove(role);
                User saved = userRepository.save(user);
            }
        }
        return getPowerUsers(userDetails,model);
    }


    @RequestMapping(value = "/getPowerUsers")
    public String getPowerUsers(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        User user =  userRepository.findUserByUsername(userDetails.getUsername());
        Role role = roleRepository.findRoleByName("POWER_USER");
        List<User> powerUsers = new ArrayList<>();
        if (role!=null){
            powerUsers = userRepository.findUsersByRolesContains(role);
        }
        if (powerUsers==null) powerUsers = new ArrayList<>();
        model.addAttribute("powerUsers",powerUsers);
        model.addAttribute("user",user);
        return "profile :: power-users-table";
    }

    /*
    @RequestMapping(value = "/getOrderedUsers",method = RequestMethod.GET)
    public String getOrderedUsers(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                  @RequestParam int order){
        User user =  userRepository.findUserByUsername(userDetails.getUsername());

        List<User> users = userRepository.findAll();
        if (users==null) users = new ArrayList<>();
        users.stream().forEach(u->u.setAmountOfQuestions(statisticsService.getAmountOfUsersQuestion(u.getId())));
        switch (order){
            case 1: users = userRepository.findAllByOrderByUsername(); Collections.reverse(users); break;
            case 2: users = userRepository.findAllByOrderByUsername(); break;
            case 3: users.sort(Comparator.comparing(u->u.getCreationdate().getTime())); break;
            case 4: users.sort(Comparator.comparing(u->u.getCreationdate().getTime()*(-1))); break;
            case 5: users.sort(Comparator.comparing(u->u.getProfession()!=null?u.getProfession().getName():""));Collections.reverse(users);  break;
            case 6: users.sort(Comparator.comparing(u->u.getProfession()!=null?u.getProfession().getName():"")); break;
            case 7: users.sort(Comparator.comparing(u->u.getAmountOfQuestions())); break;
            case 8: users.sort(Comparator.comparing(u->u.getAmountOfQuestions()*(-1))); break;
            case 9: users.sort(Comparator.comparing(u->u.getRank())); break;
            case 10: users.sort(Comparator.comparing(u->u.getRank()*(-1))); break;
        }
        model.addAttribute("users",users);
        model.addAttribute("user",user);
        return "users :: users-table";
    }

    @RequestMapping(value = "/searchUsers",method = RequestMethod.GET)
    public String getOrderedUsers(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                  @RequestParam String username){
        User user =  userRepository.findUserByUsername(userDetails.getUsername());

        List<User> users = userRepository.findUsersByUsernameLike("%"+username+"%");

        users.stream().forEach(u->u.setAmountOfQuestions(statisticsService.getAmountOfUsersQuestion(u.getId())));
        model.addAttribute("users",users);
        model.addAttribute("user",user);
        return "users :: users-table";
    }
    */

    @RequestMapping(value = "/saveUser",method = RequestMethod.POST)
    public String saveUser(@AuthenticationPrincipal UserDetails userDetails, Model model,
                           @RequestParam long id,
                           @RequestParam String username,
                           @RequestParam String email,
                           @RequestParam boolean enabled) {
        User user =  userRepository.findUserByUsername(userDetails.getUsername());
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User foundUser = optionalUser.get();
            foundUser.setUsername(username);
            foundUser.setEmail(email);
            foundUser.setEnabled(enabled);
            User saved = userRepository.save(foundUser);
        }
        List<User> users = userRepository.findAllByOrderByUsername();
        model.addAttribute("users",users);
        model.addAttribute("user",user);
        return "users :: users-table";
    }

    @RequestMapping(value = "/deleteUser",method = RequestMethod.POST)
    public String deleteUser(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                  @RequestParam long id){
        User user =  userRepository.findUserByUsername(userDetails.getUsername());
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            userRepository.delete(optionalUser.get());
        }
        List<User> users = userRepository.findAllByOrderByUsername();
        model.addAttribute("users",users);
        model.addAttribute("user",user);
        return "users :: users-table";
    }

    @RequestMapping(value = "/getUser",method = RequestMethod.GET)
    public String getUser(@AuthenticationPrincipal UserDetails userDetails, Model model,
                                  @RequestParam long id){
        User user =  userRepository.findUserByUsername(userDetails.getUsername());
        User foundUser=null;

        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            foundUser=optionalUser.get();
        }
        List<User> users = userRepository.findAllByOrderByUsername();
        model.addAttribute("foundUser",foundUser);
        return "users :: updUserModal";
    }
}
