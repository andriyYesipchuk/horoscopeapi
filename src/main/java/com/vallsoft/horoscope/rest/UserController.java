package com.vallsoft.horoscope.rest;

import com.vallsoft.horoscope.entity.User;
import com.vallsoft.horoscope.response.ApiResponse;
import com.vallsoft.horoscope.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users/")
public class UserController {
    @Autowired
    UserService userService;
    @RequestMapping
    public ApiResponse<List<User>> readAll(){
        return userService.findAll();
    }

    @RequestMapping("get/")
    public ApiResponse<User> readOne(@RequestParam int id){
        return userService.findById(id);

    }

    @RequestMapping("get_by_username/")
    public ApiResponse<List<User>> readByUsername(@RequestParam String username, long id){
        return userService.findUsersByUsername(username,id);

    }

    @RequestMapping(value = "create/", method = RequestMethod.POST)
    public ApiResponse<User> create(@RequestParam String username, @RequestParam String email, @RequestParam String password, @RequestParam String profession) {
        return userService.register(username,email,password,profession);

    }

    @RequestMapping(value = "update/", method = RequestMethod.PUT)
    public ApiResponse<User> update(@RequestBody User user){
       return userService.save(user);

    }

    @RequestMapping(value = "delete/", method = RequestMethod.DELETE)
    public ApiResponse<User> delete(@RequestParam int id){
        return userService.deleteById(id);
    }

    @RequestMapping(value = "login/", method = RequestMethod.POST)
    public ApiResponse<User> login(@RequestParam String username, @RequestParam String password){
        return userService.login(username, password);
    }

    @RequestMapping(value = "update_info/", method = RequestMethod.POST)
    public ApiResponse<User> updateInfo(@RequestParam long id, @RequestParam String username, @RequestParam String email, @RequestParam String password){
        return userService.updateInfo(id, username, email, password);
    }

}
