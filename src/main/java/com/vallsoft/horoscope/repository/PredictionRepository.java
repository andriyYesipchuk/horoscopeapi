package com.vallsoft.horoscope.repository;

import com.vallsoft.horoscope.entity.Prediction;
import com.vallsoft.horoscope.entity.PredictionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PredictionRepository extends JpaRepository<Prediction, Long> {

    Prediction findPredictionByText(String text);

    List<Prediction> getAllByType(PredictionType type);
}
