CREATE TABLE IF NOT EXISTS public."user"
(
  id bigint NOT NULL,
  creationdate timestamp without time zone,
  email character varying(255),
  enabled boolean NOT NULL,
  firstname character varying(255),
  lastname character varying(255),
  modificationdate timestamp without time zone,
  password character varying(255),
  username character varying(255),
  CONSTRAINT user_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

CREATE TABLE IF NOT EXISTS public.role
(
  id bigint NOT NULL,
  creationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  description character varying(255),
  modificationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  name character varying(255),
  CONSTRAINT role_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

CREATE TABLE IF NOT EXISTS public.user_roles
(
  user_id bigint NOT NULL,
  role_id bigint NOT NULL,
  CONSTRAINT fk55itppkw3i07do3h7qoclqd4k FOREIGN KEY (user_id)
  REFERENCES public."user" (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkrhfovtciq1l558cw6udg0h0d3 FOREIGN KEY (role_id)
  REFERENCES public.role (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

CREATE TABLE IF NOT EXISTS public.prediction_type
(
  id bigint NOT NULL,
  creationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  modificationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  name character varying(255),
  CONSTRAINT prediction_type_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

CREATE TABLE IF NOT EXISTS public.prediction
(
  id bigint NOT NULL,
  text character varying(2048),
  type_id bigint NOT NULL,
  creationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  modificationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT prediction_pkey PRIMARY KEY (id),
  CONSTRAINT fkrhfovtciq11238cw6udg0h0d3 FOREIGN KEY (type_id)
  REFERENCES public.prediction_type (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

-- auto-generated definition
create sequence  if not exists roles_sequence START 3 INCREMENT 1;

-- auto-generated definition
create sequence  if not exists  users_sequence START 2 INCREMENT 1;

-- auto-generated definition
create sequence  if not exists  prediction_types_sequence START 9 INCREMENT 1;

-- auto-generated definition
create sequence  if not exists  predictions_sequence START 9 INCREMENT 1;



