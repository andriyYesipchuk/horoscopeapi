package com.vallsoft.horoscope.rest;

import com.vallsoft.horoscope.entity.PredictionType;
import com.vallsoft.horoscope.response.ApiResponse;
import com.vallsoft.horoscope.service.PredictionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/prediction_type/")
public class PredictionTypeController {

    @Autowired
    PredictionTypeService predictionTypeService;

    @RequestMapping
    public ApiResponse<List<PredictionType>> readAll() {
        return predictionTypeService.findAll();
    }

    @RequestMapping("get/")
    public ApiResponse<PredictionType> readOne(@RequestParam int id) {
        return predictionTypeService.findById(id);

    }

    @RequestMapping(value = "create/", method = RequestMethod.POST)
    public ApiResponse<PredictionType> create(@RequestBody PredictionType predictionType) {
        return predictionTypeService.save(predictionType);

    }

    @RequestMapping(value = "update/", method = RequestMethod.PUT)
    public ApiResponse<PredictionType> update(@RequestBody PredictionType predictionType) {
        return predictionTypeService.save(predictionType);

    }

    @RequestMapping(value = "delete/", method = RequestMethod.DELETE)
    public ApiResponse<PredictionType> delete(@RequestParam int id) {
        return predictionTypeService.deleteById(id);
    }
}
