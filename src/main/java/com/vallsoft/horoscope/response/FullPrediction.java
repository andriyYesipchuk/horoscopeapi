package com.vallsoft.horoscope.response;

import com.vallsoft.horoscope.entity.Prediction;
import com.vallsoft.horoscope.entity.PredictionType;

import java.util.List;

public class FullPrediction {
    private List<PredictionType> types;
    private List<Prediction> predictions;

    public FullPrediction(List<PredictionType> types, List<Prediction> predictions) {
        this.types = types;
        this.predictions = predictions;
    }

    public FullPrediction() {
    }

    public List<PredictionType> getTypes() {
        return types;
    }

    public void setTypes(List<PredictionType> types) {
        this.types = types;
    }

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }
}